#!/usr/bin/env bash

while [ true ]; do

curl -s --user "api:$MAILGUN_API_KEY" \
      "https://api.mailgun.net/v3/$MAILGUN_DOMAIN/messages" \
      -F from='Acuparse DNS Pipeline <noreply@acuparse.com>' \
      -F to='Acuparse CI Notifications <ci_notify@acuparse.com>' \
      -F subject='CI Job Running' \
      -F text='The CI job is still running!'

sleep 30

done